local vim,lsp,api = vim,vim.lsp,vim.api
local diag = vim.diagnostic
local feline_git = require "feline.providers.git"
local feline_lsp = require "feline.providers.lsp"

local colors = {
    bg = '#444444',
    yellow = '#DCDCAA',
    dark_yellow = '#D7BA7D',
    cyan = '#4EC9B0',
    mgold = '#CE9812',
    green = '#00ff58',
    light_green = '#B5CEA8',
    string_orange = '#CE9178',
    orange = '#FFAD00',
    purple = '#CF58CF',
    magenta = '#D16D9E',
    grey = '#C5C5CC',
    blue = '#569CD6',
    vivid_blue = '#77AAFF',
    light_blue = '#99ccff',
    red = '#D16969',
    error_red = '#FF5040',
    info_yellow = '#FFCC66'
}

-- auto change color according the vim mode
local mode_color = {
    n = colors.blue,
    i = colors.mgold,
    v = colors.purple,
    [''] = colors.purple,
    V = colors.purple,
    c = colors.magenta,
    no = colors.blue,
    s = colors.orange,
    S = colors.orange,
    [''] = colors.orange,
    ic = colors.yellow,
    R = colors.red,
    Rv = colors.red,
    cv = colors.blue,
    ce = colors.blue,
    r = colors.cyan,
    rm = colors.cyan,
    ['r?'] = colors.cyan,
    ['!'] = colors.blue,
    t = colors.blue
}

local mode_names = {
    n = "  Normal ",
    i = "  Insert ",
    v = "  Visual ",
    [''] = "  V-Block",
    V = "  VISUAL ",
    R = "  Replace",
    c = "  Command"
}
vimerror = diag.severity.ERROR
vimwarn = diag.severity.WARN
vimhint = diag.severity.HINT
viminfo = diag.severity.INFO
get_diagnostics_count = function(sev)
    return vim.tbl_count(diag.get(0, {severity = sev}))
end

get_my_diags = function()
    de = tonumber(get_diagnostics_count(vimerror))
    dw = tonumber(get_diagnostics_count(vimwarn))
    dh = tonumber(get_diagnostics_count(vimhint))
    di = tonumber(get_diagnostics_count(viminfo))
    -- de = 0
    -- de = 1

    -- dh = 1
    -- di = 1
    return de, dw, dh, di
end

local lcars = {
    active = {},
    inactive = {}
}

-- Insert three sections (left, mid and right) for the active statusline
table.insert(lcars.active, {})
table.insert(lcars.active, {})
table.insert(lcars.active, {})

-- Insert two sections (left and right) for the inactive statusline
table.insert(lcars.inactive, {})
table.insert(lcars.inactive, {})

lcars.active[1][1] = {
    provider = function()
        m = vim.fn.mode()
        name = mode_names[m]
        if name == nil then
            name = m
        end
        return name
    end,
    hl = function()
        return {
            name = "test",
            fg = colors['bg'],
            bg = mode_color[vim.fn.mode()]
        }
    end,
}


lcars.active[1][2] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if de == 0 then
            return ""
        end
        return " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = mode_color[vim.fn.mode()]
        bg = colors['bg']
        if de > 0 then
            bg = colors.error_red
        end
        return {
            name = "test",
            fg = c,
            bg = bg
        }
    end

}

lcars.active[1][3] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if de == 0 then
            return ""
        end
        return tostring(de) .. " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = colors['bg']
        bg = colors.error_red
        return {
            name = "test",
            fg = c,
            bg = bg
        }
    end
}

lcars.active[1][4] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if dw == 0 then
            return ""
        end
        return " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = mode_color[vim.fn.mode()]
        bg = mode_color[vim.fn.mode()]
        if de > 0 then
            bg = colors.error_red
        end


        if dw > 0 then
            c = colors.orange
        elseif dh > 0 then
            c = colors.vivid_blue
        elseif di > 0 then
            c = colors.light_blue
        end
        return {
            name = "test",
            fg = bg,
            bg = c
        }
    end
}

lcars.active[1][5] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if dw == 0 then
            return ""
        end
        return tostring(dw) .. " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = colors['bg']
        bg = colors.orange
        return {
            name = "test",
            fg = c,
            bg = bg
        }
    end
}

lcars.active[1][6] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if dh == 0 then
            return ""
        end
        return " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = mode_color[vim.fn.mode()]
        bg = mode_color[vim.fn.mode()]
        if dw > 0 then
            bg = colors.orange
        elseif de > 0 then
            bg = colors.error_red
        end

        if dh > 0 then
            c = colors.vivid_blue
        elseif di > 0 then
            c = colors.light_blue
        else
            c = colors['bg']
        end
        return {
            name = "test",
            fg = bg,
            bg = c
        }
    end
}

lcars.active[1][7] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if dh == 0 then
            return ""
        end
        return tostring(dh) .. " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = colors['bg']
        bg = colors.vivid_blue
        return {
            name = "test",
            fg = c,
            bg = bg
        }
    end
}

lcars.active[1][8] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if di == 0 then
            return ""
        end
        return " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = mode_color[vim.fn.mode()]
        bg = mode_color[vim.fn.mode()]
        if dh > 0 then
            bg = colors.vivid_blue
        elseif dw > 0 then
            bg = colors.orange
        elseif de > 0 then
            bg = colors.error_red
        end

        if di > 0 then
            c = colors.light_blue
        else
            c = colors['bg']
        end
        return {
            name = "test",
            fg = bg,
            bg = c
        }
    end
}

lcars.active[1][9] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        if di == 0 then
            return ""
        end
        return tostring(di) .. " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = colors['bg']
        bg = colors.light_blue
        return {
            name = "test",
            fg = c,
            bg = bg
        }
    end
}

lcars.active[1][10] = {
    provider = function()
        de, dw, dh, di = get_my_diags()
        return " "
    end,
    hl = function()
        de, dw, dh, di = get_my_diags()
        c = mode_color[vim.fn.mode()]
        bg = mode_color[vim.fn.mode()]
        if di > 0 then
            bg = colors.light_blue
        elseif dh > 0 then
            bg = colors.vivid_blue
        elseif dw > 0 then
            bg = colors.orange
        elseif de > 0 then
            bg = colors.error_red
        end

        c = colors['bg']
        return {
            name = "test",
            fg = bg,
            bg = c
        }
    end
}

table.insert(lcars.active[2], {
    provider = function()
        branch = feline_git.git_branch()
        if branch == "" then
            return ""
        end
        return ""
    end,
    hl = function()
        branch = feline_git.git_branch()
        bg = colors.purple
        if branch == "main" or branch == "master" then
            bg = colors.mgold
        end
        return {
            name = "git branch",
            bg = colors['bg'],
            fg = bg,
        }
    end
})

table.insert(lcars.active[2], {
    provider = function()
        branch = feline_git.git_branch()
        return branch
    end,
    hl = function()
        if branch == "main" or branch == "master" then
            bg = colors.mgold
        end
        return {
            name = "git branch",
            fg = colors['bg'],
            bg = bg,
        }
    end
})

table.insert(lcars.active[2], {
    provider = function()
        branch = feline_git.git_branch()
        if branch == "" then
            return ""
        end
        return ""
    end,
    hl = function()
        branch = feline_git.git_branch()
        bg = colors.purple
        if branch == "main" or branch == "master" then
            bg = colors.mgold
        end
        return {
            name = "git branch",
            bg = colors.green,
            fg = bg,
        }
    end
})

-- lcars.active[2][4] = {
--     provider = function()
--         n = feline_git.git_diff_added()
--         if n ~= "" then
--             return ""
--         end
--         return ""
--     end,
--     hl = function()
--         return {
--             name = "git branch",
--             bg = colors['bg'],
--             fg = colors.green,
--         }
--     end
-- }

table.insert(lcars.active[2], {
    provider = function()
        n = feline_git.git_diff_added()
        if n ~= "" then
            return " " .. n
        end
        return " 0"
    end,
    hl = function()
        return {
            name = "git branch",
            fg = colors['bg'],
            bg = colors.green,
        }
    end
})
table.insert(lcars.active[2], {
    provider = function()
        n = feline_git.git_diff_added()
        branch = feline_git.git_branch()
        return ""
    end,
    hl = function()
        branch = feline_git.git_branch()
        bg = colors.purple
        return {
            name = "git branch",
            bg = colors.orange,
            fg = colors.green,
        }
    end
})

-- lcars.active[2][7] = {
--     provider = function()
--         n = feline_git.git_diff_changed()
--         if n ~= "" then
--             return ""
--         end
--         return ""
--     end,
--     hl = function()
--         return {
--             name = "git branch",
--             bg = colors['bg'],
--             fg = colors.orange,
--         }
--     end
-- }
--
table.insert(lcars.active[2], {
    provider = function()
        n = feline_git.git_diff_changed()
        if n ~= "" then
            return " " .. n
        end
        return " 0"
    end,
    hl = function()
        return {
            name = "git branch",
            fg = colors['bg'],
            bg = colors.orange,
        }
    end
})
table.insert(lcars.active[2], {
    provider = function()
        n = feline_git.git_diff_changed()
        return ""
    end,
    hl = function()
        return {
            name = "git branch",
            bg = colors.red,
            fg = colors.orange,
        }
    end
})


table.insert(lcars.active[2], {
    provider = function()
        n = feline_git.git_diff_removed()
        if n ~= "" then
            return " " .. n
        end
        return " 0"
    end,
    hl = function()
        return {
            name = "git branch",
            fg = colors['bg'],
            bg = colors.red,
        }
    end
})
table.insert(lcars.active[2], {
    provider = function()
        n = feline_git.git_diff_removed()
        return ""
    end,
    hl = function()
        return {
            name = "git branch",
            bg = colors['bg'],
            fg = colors.red,
        }
    end
})

table.insert(lcars.active[3], {
        provider = function()
            return ""
        end,
        hl = function()
            return {
                name = "clients",
                bg = colors['bg'],
                fg = colors.light_blue
            }
        end
    })

table.insert(lcars.active[3], {
        provider = function()
            clients = feline_lsp.lsp_client_names()
            if string.len(clients) == 0 then
                return "none "
            end
            return clients .. " "
        end,
        hl = function()
            return {
                name = "clients",
                fg = colors['bg'],
                bg = colors.light_blue
            }
        end
    })
-- │

table.insert(lcars.active[3], {
        provider = function()
            sw = tostring(vim.api.nvim_buf_get_option(0, "shiftwidth"))
            ts = tostring(vim.api.nvim_buf_get_option(0, "tabstop"))
            local line = vim.fn.line('.')
            local col = vim.fn.col('.')
            pos = string.format("%4d :%3d ", line, col)
            return "│sw:" .. sw .. " ts:" .. ts .. "│" .. pos
        end,
        hl = function()
            return {
                name = "clients",
                fg = colors['bg'],
                bg = colors.light_blue
            }
        end
    })

require('feline').setup{
    components=lcars
}
