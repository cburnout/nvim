local pid = vim.fn.getpid()
-- On linux/darwin if using a release build, otherwise under scripts/OmniSharp(.Core)(.cmd)
local omnisharp_bin = "/home/cburn/repos/omnisharp-roslyn/artifacts/scripts/OmniSharp.Stdio"
-- on Windows
-- local omnisharp_bin = "/path/to/omnisharp/OmniSharp.exe"
lsp = lsp or require'lsp.base-config'
require'lspconfig'.omnisharp.setup{
    cmd = { omnisharp_bin, "--languageserver" , "--hostPID", tostring(pid) },
    on_attach = lsp.on_attach,
}
