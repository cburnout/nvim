local nvim_lsp = require'lspconfig'

lsp = require'lsp.base-config'

nvim_lsp.rust_analyzer.setup({
    on_attach=lsp.on_attach,
    settings = {
        ["rust-analyzer"] = {
            imports = {
                granularity = {
                    group = "module",
                },
                prefix = "self",
            },
            cargo = {
                buildScripts = {
                    enable = true,
                },
            },
            procMacro = {
                enable = false,
            },
        }
    }
})

