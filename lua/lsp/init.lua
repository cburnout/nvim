require('lsp/base-config')
require('lsp/python-lsp')
require('lsp/lua-lsp')
require('lsp/omnisharp-lsp')
require('lsp/julia')
require('lsp/flutter')
require('lsp/marksman')
require('lsp/rust')
require('lsp/latex')

return M
