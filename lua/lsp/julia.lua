bc = require'lsp.base-config'
require'lspconfig'.julials.setup{
  bc = require'lsp.base-config'
}
local nvim_lsp = require('lspconfig')

nvim_lsp['julials'].setup {
    on_attach = bc.on_attach,
    flags = {
      debounce_text_changes = 150,
    }
}
